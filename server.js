var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var config      = require('./config/database'); // get db config file
var port        = process.env.PORT || 8080;

// get our request parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// log to console
app.use(morgan('dev'));

// connect to database
mongoose.connect(config.database);

usersroute = require("./app/apis/users");
app.use('/api/user', usersroute); 

eventsroute = require("./app/apis/events");
app.use('/api/event', eventsroute); 

// Start the server
app.listen(port);
console.log('There will be dragons: http://localhost:' + port);