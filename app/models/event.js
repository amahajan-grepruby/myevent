var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
  
// set up a mongoose model
var EventSchema = new Schema({

    title: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String
    },
    startDate: {
        type: Date,
        required: true,
        default: Date.now
    },
    endDate: {
        type: Date,
        required: true
    },
    address: {
        type: String
    },
    city: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    postalCode: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    logo: String,
    userId: String
});
 
EventSchema.pre('save', function (next) {
    var event = this;
    if(this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            next();
        });
    } else {
        return next();
    }
    
});
 
module.exports = mongoose.model('Event', EventSchema);