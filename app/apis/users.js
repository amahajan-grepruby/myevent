var express     = require('express');
var app         = express();
var User        = require('../../app/models/user'); 
var jwt         = require('jwt-simple');
var config      = require('../../config/database');
var passport    = require('passport');
var nodemailer  = require('nodemailer');

// Use the passport package in our application
app.use(passport.initialize());

// pass passport for configuration
require('../../config/passport')(passport);

// bundle our routes
var apiRoutes   = express.Router();

/*
REQUEST TYPE :: POST
URL :: /api/signup
Parameters :: name (unique & required), password(required), confirmpassword(required), email(unique & required)
Desc :: Create new User
*/
// create a new user account (POST {SITEURL}:8080/api/signup)
apiRoutes.post('/signup', function(req, res) {
    if (!req.body.name || !req.body.password || !req.body.confirmpassword) {
        return res.status(403).send({success: false, msg: 'Please pass name and passwords.'});
    } 
    if(req.body.password != req.body.confirmpassword) {
        return res.status(403).send({success: false, msg: 'Password & Confirm Password do not match'});
    } else {
        var newUser = new User({
            name: req.body.name,
            password: req.body.password,
            email: req.body.email,
            phone: req.body.phone
        });
        // save the user
        newUser.save(function(err) {
            if (err) {
                return res.status(403).send({success: false, msg: 'Username already exists.'});
            }
            res.json({success: true, msg: 'Successful created new user.'});
        });
    }
});

/* ============================================== */

/*
REQUEST TYPE :: POST
URL :: /api/user/authenticate
Parameters :: name (required), password(required)
Desc :: Login API
*/
// route to authenticate a user (POST {SITEURL}:8080/api/user/authenticate)
apiRoutes.post('/authenticate', function(req, res) {
    User.findOne({
        name: req.body.name
    }, function(err, user) {
        if (err) throw err;
 
        if (!user) {
            res.send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
            // check if password matches
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token
                    var token = jwt.encode(user, config.secret);
                    // return the information including token as JSON
                    res.json({success: true, token: 'JWT ' + token});
                } else {
                    res.status(403).send({success: false, msg: 'Authentication failed. Wrong password.'});
                }
            });
        }
    });
});

/* ============================================== */

/*
REQUEST TYPE :: GET
URL :: /api/user/memberinfo
HEADER :: AUTHORIZATION : {JWT Token}
Desc :: Get logged-in user info
*/
// route to a restricted info (GET {SITEURL}:8080/api/user/memberinfo)
apiRoutes.get('/memberinfo', passport.authenticate('jwt', { session: false}), function(req, res) {
    var token = getToken(req.headers);
    if (token) {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({
            name: decoded.name
        }, function(err, user) {
            if (err) throw err;
 
            if (!user) {
                return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
            } else {
                res.json({success: true, user: user});
            }
        });
    } else {
        return res.status(403).send({success: false, msg: 'No token provided.'});
    }
});

/* ============================================== */

/*
REQUEST TYPE :: POST
URL :: /api/user/forgotpassword
Parameters :: email(required)
Desc :: Used to send an email link for forgotten password
*/ 
// route to a forgot password info (POST {SITEURL}:8080/api/user/forgotpassword)
apiRoutes.post('/forgotpassword', function(req, res) {

    if (!req.body.email) {
        res.status(403).send({success: false, msg: 'Please pass email.'});
    } else {

        User.findOne({
            email: req.body.email
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.status(403).send({success: false, msg: 'Authentication failed. User not found.'}); 
            } else {

                var randomstring = require("randomstring");
                var token = randomstring.generate();
                user.resetPasswordToken = token;
                user.save(function(err) {
                    if (err) {
                        return res.json({success: false, msg: err});
                    }
                });
                var transporter = nodemailer.createTransport({
                    service: 'Gmail',
                    auth: {
                        user: 'testmail.grepruby@gmail.com', // Your email id
                        pass: 'success123' // Your password
                    }
                });
                var mailOptions = {
                    from: 'testmail.grepruby@gmail.com', // sender address
                    to: req.body.email, // list of receivers
                    subject: 'Forgot Password Mail', // Subject line
                    //text: 'test mail.... Token - '+token //, // plaintext body
                    html: 'Hi, <br/> URL to reset your password is as follows : <br/> http://localhost:8080/api/user/forgotpasswordtoken?token='+token
                };
                transporter.sendMail(mailOptions, function(error, info){
                    if(error){
                        res.status(403).send({success: false, msg: 'error'});
                    } else {
                        res.json({success:true, msg: "Mail Sent"});
                    };
                });
            }
        });
    }
});

/* ============================================== */

/*
REQUEST TYPE :: GET
URL :: /api/user/forgotpasswordtoken
Parameters :: token(required)
Desc :: Forgot password email will have one token and this api will authenticate that token. 
Token will only be used once.
Response will again have new token(JWT) that can be used further to reset password
*/ 
// route to validate forgot password token (GET http://localhost:8080/api/user/forgotpasswordtoken)
apiRoutes.get('/forgotpasswordtoken', function(req, res) {
    if(!req.query.token) {
        return res.send({success: false, msg: 'Pass token'});
    }  
    User.findOne({
        resetPasswordToken: req.query.token
    }, function(err, user) {
        if (err) throw err;
 
        if (!user) {
            res.status(403).send({success: false, msg: 'Token is not valid.'});
        } else {
            user.resetPasswordToken = '';
            user.save(function(err) {
                if (err) {
                    return res.status(403).json({success: false, msg: err});
                }
            });
            var token = jwt.encode(user, config.secret);
            res.json({success: true, token: 'JWT ' + token});
        }
    });
});

/* ============================================== */

/*
REQUEST TYPE :: POST
URL :: /api/user/changepassword
HEADER :: AUTHORIZATION : {JWT Token}
Parameters :: newpassword(required), confirmpassword(required)
Desc :: After forgotpasswordtoken api hit this api including JWT Token in header will allows you set new passwords
*/
// route to a change password info (POST {SITE URL}:8080/api/user/changepassword)
apiRoutes.post('/changepassword', passport.authenticate('jwt', { session: false}), function(req, res) {
    if(!req.body.newpassword || !req.body.confirmpassword) {
        res.status(403).send({success: false, msg: 'Please pass Password & Confirm Password'});
    } else {
        if (req.body.newpassword != req.body.confirmpassword) {
            res.status(403).send({success: false, msg: 'Password & Confirm Password do not match.'});
        } else {
            var token = getToken(req.headers);
            if (token) {
                var decoded = jwt.decode(token, config.secret);
                User.findOne({
                    name: decoded.name
                }, function(err, user) {
                    if (err) throw err;

                    if (!user) {
                        res.send({success: false, msg: 'Authentication failed. User not found.'});
                    } else {
                        user.password = req.body.newpassword;
                        user.save(function(err) {
                            if (err) {
                                return res.status(403).send({success: false, msg: err});
                            }
                        res.json({success: true, msg: 'Successful updated user.'});
                        });
                    }
                });
            }
        }
    }
});

/* ============================================== */

/*
REQUEST TYPE :: POST
URL :: /api/user/updatepassword
HEADER :: AUTHORIZATION : {JWT Token}
Parameters :: password(required){Current Password}, newpassword(required), confirmpassword(required)
Desc :: After login, if user wants to update ther password then can use this api
*/
// route to a update password after login (POST {SiteURL}:8080/api/user/updatepassword)
apiRoutes.post('/updatepassword', passport.authenticate('jwt', { session: false}), function(req, res) {
    if(!req.body.password || !req.body.newpassword || !req.body.confirmpassword) {
        res.status(403).send({success: false, msg: 'Please pass Password & Confirm Password'});
    } else {
        if (req.body.newpassword != req.body.confirmpassword) {
            res.status(403).send({success: false, msg: 'Password & Confirm Password do not match.'});
        } else {
            var token = getToken(req.headers);
            if (token) {
                var decoded = jwt.decode(token, config.secret);
                User.findOne({
                    name: decoded.name
                }, function(err, user) {
                    if (err) throw err;

                    if (!user) {
                        res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
                    } else {
                      //Match User Password
                        user.comparePassword(req.body.password, function (err, isMatch) {
                            if (isMatch && !err) {
                                // if user is found and password is right then update a password
                                user.password = req.body.newpassword;
                                user.save(function(err) {
                                    if (err) {
                                        return res.status(403).json({success: false, msg: err.toString()});
                                    }
                                    res.json({success: true, msg: 'Successfully updated password.'});
                                });

                            } else {
                                res.status(403).send({success: false, msg: 'Password do not match.'});
                            }
                        });
                    }
                });
            } else {
                // return the information including token as JSON
                res.json({success: true, token: 'JWT ' + token});
            }
        }
    }
});

/* ============================================== */

getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

module.exports = apiRoutes;