var express     = require('express');
var app         = express();
var Event       = require('../../app/models/event'); // get the mongoose model
var jwt         = require('jwt-simple');
var config      = require('../../config/database');
var passport    = require('passport');
var multer  	= require('multer');
var storage 	= multer.diskStorage({
  	destination: function (req, file, cb) {
    	cb(null, './app/uploads')
  	},
  	filename: function (req, file, cb) {
    	cb(null, file.fieldname + '-' + Date.now())
  	}
});
var upload = multer({ storage: storage });

// Use the passport package in our application
app.use(passport.initialize());

// pass passport for configuration
require('../../config/passport')(passport);

// bundle our routes
var apiRoutes   = express.Router();

/*
REQUEST TYPE :: POST
URL :: /api/event/add
HEADER :: AUTHORIZATION : {JWT Token}
Parameters :: title (required), startDate(required){Format - YYYY-mm-dd}, endDate(required){Format - YYYY-mm-dd},
description, address, city(required), country(required), postalCode(required)
Desc :: Create new Event by logged-in user
*/

// route to a add new event (POST {SITEURL}:8080/api/event/add)
apiRoutes.post('/add', passport.authenticate('jwt', { session: false}), function(req, res) {
	if (!req.body.title || !req.body.startDate || !req.body.endDate || !req.body.address 
		|| !req.body.city || !req.body.country || !req.body.postalCode) {
    	return res.status(403).json({success: false, msg: 'Please pass required parameters.'});
  	}	 
	var token = getToken(req.headers);
	if (token) {
		var decoded = jwt.decode(token, config.secret);
		var newEvent = new Event({
			title: req.body.title,
			description: req.body.description,
			startDate: req.body.startDate,
			endDate: req.body.endDate,
			address: req.body.address,
			city: req.body.endDate,
			country: req.body.country,
			postalCode: req.body.postalCode,
			userId: decoded._id
		});
	    // save the user
	    newEvent.save(function(err) {
	      	if (err) {
	        	return res.status(403).send({success: false, msg: err.toString()});
	      	}
	      	res.json({success: true, msg: 'New Event Registered.'});
	    });
  	} else {
    	return res.status(403).send({success: false, msg: 'No token provided.'});
  	}
});

/* ============================================== */

/*
REQUEST TYPE :: POST
URL :: /api/event/update
HEADER :: AUTHORIZATION : {JWT Token}
Parameters :: title (required), startDate(required){Format - YYYY-mm-dd}, endDate(required){Format - YYYY-mm-dd},
description, address, city(required), country(required), postalCode(required), eventID(required)
Desc :: Update Existing Event
*/
// route to a update existing event (POST http://localhost:8080/api/event/update)
apiRoutes.post('/update', passport.authenticate('jwt', { session: false}), function(req, res) {
	if (!req.body.title || !req.body.startDate || !req.body.endDate || !req.body.address 
		|| !req.body.city || !req.body.country || !req.body.postalCode || !req.body.eventId) {
    	return res.status(403).json({success: false, msg: 'Please pass required parameters.'});
  	}
 
	var token = getToken(req.headers);
	if (token) {
	  	Event.findOne({
		    _id: req.body.eventId
		}, function(err, event) {
		    if (err) throw err;
		 
		    if (!event) {
		      res.send({success: false, msg: 'Please provide correct Info'});
		    } else {
		    	event.title = req.body.title;
				event.description = req.body.description;
				event.startDate = req.body.startDate;
				event.endDate = req.body.endDate;
				event.address = req.body.address;
				event.city = req.body.endDate;
				event.country = req.body.country;
				event.postalCode = req.body.postalCode;
		   	    event._id = req.body.eventId;

			    event.save(function(err) {
			        if (err) {
			        	return res.status(403).send({success: false, msg: err.toString()});
			      	}
			      	res.json({success: true, msg: 'Update Event.'});	
			    });
		    }
		});

	} else {
	    return res.status(403).send({success: false, msg: 'No token provided.'});
	}
});

/* ============================================== */

/*
REQUEST TYPE :: POST
URL :: /api/event/uploadlogo
HEADER :: AUTHORIZATION : {JWT Token}
Parameters :: logo(required), eventID(required)
Desc :: Upload logo for any existing Event
*/
// route to upload event image (POST {SITEURL}:8080/api/event/uploadlogo)
apiRoutes.post('/uploadlogo', upload.single('logo'), passport.authenticate('jwt', { session: false}), function (req, res, next) {
	
	if(!req.body.eventId) {
    	return res.status(403).json({success: false, msg: 'Please pass required parameters.'});
  	}

  	Event.findOne({
	    _id: req.body.eventId
	}, function(err, event) {
	    if (err) throw err;
	 
	    if (!event) {
	      res.status(403).send({success: false, msg: 'Please provide correct Info'});
	    } else {
	    	event.logo = req.file.path;
	   	    event._id = req.body.eventId;
		    event.save(function(err) {
		        if (err) {
		        	return res.status(403).send({success: false, msg: err.toString()});
		      	}
		      	res.json({success: true, msg: 'Event Image Uploaded successfully.'});	
		    });
	    }
	});

});

/* ============================================== */

/*
REQUEST TYPE :: GET
URL :: /api/event/list
HEADER :: AUTHORIZATION : {JWT Token}
Parameters :: page
Desc :: List all created events
*/
// route to a LIST all events (GET {SITEURL}:8080/api/event/list)
apiRoutes.get('/list', passport.authenticate('jwt', { session: false}), function(req, res) {	 
	var token = getToken(req.headers);
	if(token) {
	  	if(req.query.page && req.query.page > 0) {
	  		var pageDisplay = req.query.page - 1;	
	  	} else {
	  		var pageDisplay = 0;
	  	}
	    
	    var perPage = config.pagelimit;
	    var page = pageDisplay > 0 ? pageDisplay : 0;
	    Event.find().
		limit(perPage).
		skip(perPage * page).
		sort('id').
		exec(function (err, events) {
			if (err) { throw err;}

			Event.count().exec(function(err, count) {
	            response = {events: events, total: count};
	            res.json({success: true, response: response});
	        });
		});
  	} else {
    	return res.status(403).send({success: false, msg: 'No token provided.'});
  	}
});

/* ============================================== */

/*
REQUEST TYPE :: GET
URL :: /api/event/list
HEADER :: AUTHORIZATION : {JWT Token}
Parameters :: page
Desc :: List all upcoming events
*/
// route to a list upcoming events (GET {SITEURL}:8080/api/event/upcoming)
apiRoutes.get('/upcoming', passport.authenticate('jwt', { session: false}), function(req, res) {	 
  var token = getToken(req.headers);
  if(token) {
  	if(req.query.page && req.query.page > 0) {
  		var pageDisplay = req.query.page - 1;	
  	} else {
  		var pageDisplay = 0;
  	}
    
    var perPage = config.pagelimit;
    var page = pageDisplay > 0 ? pageDisplay : 0;
    Event.find({
		startDate: {
		    $gte: new Date()		  
		}
	}).
	limit(perPage).
	skip(perPage * page).
	sort('id').
	exec(function (err, events) {
		if (err) { throw err;}

		Event.count().exec(function(err, count) {
            response = {events: events, total: count};
            res.json({success: true, response: response});
        });
	});

  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
});

/* ============================================== */

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = apiRoutes;